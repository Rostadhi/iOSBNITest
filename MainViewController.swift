//
//  MainViewController.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 21/09/23.
//

import UIKit

class MainViewController: UITabBarController{
    
    let deepLinkURLString = "sample.id://transfer/result/?title=SUKSES&transactionCode=RF001-204"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        if let deepLinkURL = URL(string: deepLinkURLString) {
            let viewController1 = UINavigationController(rootViewController: QRISPageViewController())
            let viewController2 = UINavigationController(rootViewController: MobileAppPromoViewController())
            let viewController3 = UINavigationController(rootViewController: MobileAppPortoViewController())
            let viewController4 = UINavigationController(rootViewController: DetailPushNotificationViewController(deepLinkURL: deepLinkURL))
            
            
            viewController1.tabBarItem.image = UIImage(systemName: "qrcode")
            viewController2.tabBarItem.image = UIImage(systemName: "hand.thumbsup")
            viewController3.tabBarItem.image = UIImage(systemName: "doc")
            viewController4.tabBarItem.image = UIImage(systemName: "bubble.left.and.exclamationmark.bubble.right.fill")
            
            setViewControllers([viewController1, viewController2, viewController3, viewController4], animated: true)
        }
        
        tabBar.tintColor = .label
        
        NotificationCenter.default.addObserver(self, selector: #selector(receivedPushNotification(_:)), name:
                                                Notification.Name("PushNotificationReceived"), object: nil)
    }
    
    @objc func receivedPushNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo as? [String: Any] {
            PushNotificationService.shared.handleReceivedNotification(userInfo: userInfo)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

