//
//  FirebaseManager.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 21/09/23.
//

import Firebase

class FCMTokenManager {
    static let shared = FCMTokenManager()

    var fcmToken: String? {
        didSet {
            // Simpan token ke UserDefaults setiap kali nilainya berubah
            UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
        }
    }

    private init() {
        // Dapatkan token FCM saat aplikasi pertama kali dimuat
        Messaging.messaging().token { [weak self] token, _ in
            self?.fcmToken = token
        }
    }
}
