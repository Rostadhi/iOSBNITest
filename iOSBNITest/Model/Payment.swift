//
//  Payment.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import Foundation

struct TransactionModel {
    var bankName: String
    var merchantName: String
    var transactionNominal: String
    var transactionId: String
    
    init(bankName: String, merchantName: String, transactionNominal: String, transactionId: String) {
        self.bankName = bankName
        self.merchantName = merchantName
        self.transactionNominal = transactionNominal
        self.transactionId = transactionId
    }
}

struct NominalModel {
    var nominalId: String
    var currentNominal: Int
    
    init(nominalId: String, currentNominal: Int) {
        self.nominalId = nominalId
        self.currentNominal = currentNominal
    }
}
