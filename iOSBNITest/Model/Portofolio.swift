//
//  Portofolio.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import Foundation

struct PortofolioTransaction {
    let transactionDate: String
    let nominal: Double
}

struct PortofolioItem {
    let label: String
    let percentage: Double
    let data: [PortofolioTransaction]
}

struct PortofolioData {
    let donutChartItems: [PortofolioItem]
    let lineChartMonthData: [Int]
}
