//
//  PushNotif.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 24/09/23.
//

import Foundation

struct PushNotif {
    let title: String
    let transactionCode: String
}
