//
//  MobileAppPortoViewController.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 21/09/23.
//

import UIKit
import Charts

class MobileAppPortoViewController: UIViewController, ChartViewDelegate {
    
    var donutChart = PieChartView()
    var lineChart = LineChartView()
    var scrollView = UIScrollView()
    var viewModel: PortofolioViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Portofolio"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        donutChart.delegate = self
        lineChart.delegate = self
        
        let donutChartItems = [
            PortofolioItem(
                label: "Tarik Tunai",
                percentage: 55.0,
                data: [
                    PortofolioTransaction(transactionDate: "21/01/2023", nominal: 1000000),
                    PortofolioTransaction(transactionDate: "20/01/2023", nominal: 500000),
                    PortofolioTransaction(transactionDate: "19/01/2023", nominal: 1000000)
                ]
            ),
            PortofolioItem(
                label: "QRIS Payment",
                percentage: 31.0,
                data: [
                    PortofolioTransaction(transactionDate: "21/01/2023", nominal: 159000),
                    PortofolioTransaction(transactionDate: "20/01/2023", nominal: 35000),
                    PortofolioTransaction(transactionDate: "19/01/2023", nominal: 1500)
                ]
            ),
            PortofolioItem(
                label: "Topup Gopay",
                percentage: 7.7,
                data: [
                    PortofolioTransaction(transactionDate: "21/01/2023", nominal: 200000),
                    PortofolioTransaction(transactionDate: "20/01/2023", nominal: 195000),
                    PortofolioTransaction(transactionDate: "19/01/2023", nominal: 5000000)
                ]
            ),
            PortofolioItem(
                label: "Lainnya",
                percentage: 6.3,
                data: [
                    PortofolioTransaction(transactionDate: "21/01/2023", nominal: 1000000),
                    PortofolioTransaction(transactionDate: "20/01/2023", nominal: 500000),
                    PortofolioTransaction(transactionDate: "19/01/2023", nominal: 1000000)
                ]
            )
        ]
        
        let lineChartMonthData = [3, 7, 8, 10, 5, 10, 1, 3, 5, 10, 7, 7]
        let portofolioData = PortofolioData(donutChartItems: donutChartItems, lineChartMonthData: lineChartMonthData)
        viewModel = PortofolioViewModel(portofolioData: portofolioData)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        view.addSubview(scrollView)
        donutChart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
        donutChart.center = CGPoint(x: view.center.x, y: donutChart.frame.size.height / 2)
        let lineChartHeight = self.view.frame.size.height - donutChart.frame.maxY - 20
        lineChart.frame = CGRect(x: 0, y: donutChart.frame.maxY + 20, width: self.view.frame.size.width, height: lineChartHeight)
        scrollView.addSubview(donutChart)
        scrollView.addSubview(lineChart)
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: lineChart.frame.maxY + 20)
        
        
        if let viewModel = viewModel {
                viewModel.configureDonutChart(donutChart)
                viewModel.configureLineChart(lineChart)
        }
        
    }
}
