import Foundation
import Charts

class PortofolioViewModel {

    var portofolioData: PortofolioData
    
    init(portofolioData: PortofolioData) {
        self.portofolioData = portofolioData
    }

    func configureDonutChart(_ chart: PieChartView) {
        var entries = [ChartDataEntry]()
        var labels = [String]()

        for item in portofolioData.donutChartItems {
            let sum = item.data.reduce(0) { $0 + $1.nominal }
            let entry = PieChartDataEntry(value: Double(sum), label: nil) // Tidak perlu mengatur label di sini
            entry.label = item.label // Set label pada entry
            entries.append(entry)
        }

        let dataSet = PieChartDataSet(entries: entries, label: "")
        dataSet.colors = ChartColorTemplates.vordiplom()
        dataSet.drawValuesEnabled = true

        let data = PieChartData(dataSet: dataSet)
        chart.data = data
    }

    func configureLineChart(_ chart: LineChartView) {
        var entries = [ChartDataEntry]()

        for (index, monthValue) in portofolioData.lineChartMonthData.enumerated() {
            entries.append(ChartDataEntry(x: Double(index), y: Double(monthValue)))
        }

        let dataSet = LineChartDataSet(entries: entries, label: "Data Bulanan")
        dataSet.colors = [NSUIColor.blue]
        dataSet.drawCirclesEnabled = false

        let data = LineChartData(dataSet: dataSet)
        chart.data = data
    }
}
