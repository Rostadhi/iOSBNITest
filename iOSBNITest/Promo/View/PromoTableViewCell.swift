import UIKit

class PromoTableViewCell: UITableViewCell {
    
    static let identifier = "PromoCell"
    
    public let promoImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.layer.cornerRadius = 15
        return image
    }()
    
    public let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        contentView.addSubview(promoImage)
        contentView.addSubview(nameLabel)
        contentView.backgroundColor = .white
        
        NSLayoutConstraint.activate([
            promoImage.topAnchor.constraint(equalTo: contentView.topAnchor),
            promoImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            promoImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            promoImage.heightAnchor.constraint(equalToConstant: 180),
            
            nameLabel.topAnchor.constraint(equalTo: promoImage.bottomAnchor, constant: 10), // Add spacing below promoImage
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10), // Add spacing above the bottom of the cell
        ])
    }
    
    func getImageFromURL(imageURL: String?){
        if let imageURL = imageURL {
            guard let CompleteURL = URL(string: imageURL) else {return}
            self.promoImage.sd_setImage(with: CompleteURL)
        }
    }
}
