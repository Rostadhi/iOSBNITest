//
//  DetailPromoViewModel.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import UIKit

class DetailPromoViewModel{
    var promo: PromoElement
    var id: Int
    var name, desc: String
    var imgURL: URL?
    
    init(promo: PromoElement){
        self.promo = promo
        self.id = promo.id ?? 0
        self.name = promo.nama ?? ""
        self.desc = promo.desc ?? ""
        self.imgURL = convertImgToURL(promo.img?.formats.medium.url ?? "")
    }
    
    private func convertImgToURL(_ imgURL: String) -> URL?{
        return URL(string: imgURL)
    }
}
