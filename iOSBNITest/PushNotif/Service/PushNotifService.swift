import UIKit
import UserNotifications

class PushNotificationService: NSObject {
    static let shared = PushNotificationService()

    func configure() {
        UNUserNotificationCenter.current().delegate = self
    }

    func sendPushNotification(with payload: [String: Any]) {
        
    }

    func handleReceivedNotification(userInfo: [AnyHashable: Any]) {
        if let deepLinkString = userInfo["deepLink"] as? String,
           let deepLinkURL = URL(string: deepLinkString) {
            let detailViewController = DetailPushNotificationViewController(deepLinkURL: deepLinkURL)
        }
    }
}

extension PushNotificationService: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
            handleReceivedNotification(userInfo: userInfo)
        }
        completionHandler()
    }
}
