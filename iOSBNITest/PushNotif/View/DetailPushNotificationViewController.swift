//
//  DetailPushNotificationViewController.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 24/09/23.
//

import Foundation
import UIKit

class DetailPushNotificationViewController: UIViewController {
    let viewModel: DetailViewModel
    
    init(deepLinkURL: URL) {
        self.viewModel = DetailViewModel(deepLinkURL: deepLinkURL)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Push Notification"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        setupUI()
    }
    
    private func setupUI() {
        let titleLabel = UILabel()
        titleLabel.text = "Judul: \(viewModel.title ?? "")"
        titleLabel.textAlignment = .center
        
        let transactionCodeLabel = UILabel()
        transactionCodeLabel.text = "Transaction Code: \(viewModel.transactionCode ?? "")"
        transactionCodeLabel.textAlignment = .center
        
        view.addSubview(titleLabel)
        view.addSubview(transactionCodeLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        transactionCodeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
            
            transactionCodeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            transactionCodeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20)
        ])
    }
}
