//
//  DetailViewModel.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 24/09/23.
//

import Foundation

class DetailViewModel {
    var title: String?
    var transactionCode: String?

    init(deepLinkURL: URL) {
        if let components = URLComponents(url: deepLinkURL, resolvingAgainstBaseURL: false) {
            if let titleValue = components.queryItems?.first(where: { $0.name == "Judul" })?.value {
                title = titleValue
            }
            if let transactionCodeValue = components.queryItems?.first(where: { $0.name == "transactionCode" })?.value {
                transactionCode = transactionCodeValue
            }
        }
    }
}
