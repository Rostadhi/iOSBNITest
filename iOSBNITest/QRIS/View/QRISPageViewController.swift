//
//  QRISPageViewController.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 21/09/23.
//
import UIKit

class QRISPageViewController: UIViewController {
    
    lazy var goToScannerPageButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Scan QRIS", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemBlue
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goToScannerPage(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var goToHistoryTransactionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Riwayat Transaksi", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemBlue
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goToHistoryTransaction(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "QRIS Scanner"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        setupLayout()
        
    }
    
    func setupLayout(){
        view.addSubview(goToScannerPageButton)
        view.addSubview(goToHistoryTransactionButton)
        view.backgroundColor = .white
        
        NSLayoutConstraint.activate([
            
            goToScannerPageButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            goToScannerPageButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            goToHistoryTransactionButton.topAnchor.constraint(equalTo: goToScannerPageButton.bottomAnchor, constant: 16),
            goToHistoryTransactionButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
        ])
        
    }
    
    @objc func goToScannerPage(_ sender: UIButton){
        let scannerVC = QRISScannerViewController()
        self.navigationController?.pushViewController(scannerVC, animated: true)
    }
    
    @objc func goToHistoryTransaction(_ sender: UIButton){
        let vc = QRISHistoryTransactionViewController()
        let navigationController = UINavigationController(rootViewController: vc)
        self.present(navigationController, animated: true, completion: nil)
    }
}
