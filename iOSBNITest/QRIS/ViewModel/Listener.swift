//
//  Listener.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import UIKit

class Listener<T>{
    var value: T? {
        didSet {
            DispatchQueue.main.async {
                self.listener?(self.value)
            }
        }
    }
    
    init(_ value: T?) {
        self.value = value
    }
    
    private var listener: ((T?) -> Void)?
    
    func bind(_ listener: @escaping(T?) -> Void){
        listener(value)
        self.listener = listener
    }
}
