//
//  PaymentViewModel.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import UIKit

class PaymentViewModel {
    
    var dataSource: TransactionModel
    var transactionNominal: String
    var bankName: String
    var merchantName: String
    var transactionId: String
    
    init(dataSource: TransactionModel){
        self.dataSource = dataSource
        self.bankName = dataSource.bankName
        self.merchantName = dataSource.merchantName
        self.transactionId = dataSource.transactionId
        self.transactionNominal = dataSource.transactionNominal
    }
    
}

