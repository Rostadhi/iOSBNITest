//
//  QRISScanViewModel.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 22/09/23.
//

import UIKit

class QRISScanViewModel{

    func getDataFromQR(result: String) -> TransactionModel?{
        let dataArr = result.components(separatedBy: ".")
        let transaction = TransactionModel(bankName: dataArr[0], merchantName: dataArr[1], transactionNominal: dataArr[2], transactionId: dataArr[3])
        
        return transaction
    }

}
