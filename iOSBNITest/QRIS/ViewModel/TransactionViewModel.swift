//
//  TransactionViewModel.swift
//  iOSBNITest
//
//  Created by Rostadhi Akbar on 23/09/23.
//

import UIKit
import CoreData

class TransactionViewModel {
    
    func createTransaction(_ transactionNominal:String, _ bankName:String, _ merchantName:String, _ transactionId:String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let transactionEntity = NSEntityDescription.entity(forEntityName: "TransactionData", in: managedContext)
        let insert = NSManagedObject(entity: transactionEntity!, insertInto: managedContext)
        
        insert.setValue(transactionNominal, forKey: "transactionNominal")
        insert.setValue(bankName, forKey: "bankName")
        insert.setValue(merchantName, forKey: "merchantName")
        insert.setValue(transactionId, forKey: "transactionId")
        
        do{
            try managedContext.save()
        }catch let err{
            print(err)
        }
        
    }
    
    func retrieveTransaction() -> [TransactionModel]{
        var transactions = [TransactionModel]()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        
        do{
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            result.forEach{ data in
                transactions.append(
                    TransactionModel(bankName: data.value(forKey: "bankName") as! String, merchantName: data.value(forKey: "merchantName") as! String, transactionNominal: data.value(forKey: "transactionNominal") as! String, transactionId: data.value(forKey: "transactionId") as! String)
                )
            }
        }catch let err{
            print(err)
        }
        
        return transactions
        
    }
    
    func createNewNominal(_ currentNominal: Int, _ nominalId: String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let transactionEntity = NSEntityDescription.entity(forEntityName: "CurrentNominal", in: managedContext)
        let insert = NSManagedObject(entity: transactionEntity!, insertInto: managedContext)
        
        insert.setValue(currentNominal, forKey: "currentNominal")
        insert.setValue(nominalId, forKey: "nominalId")
        
        do{
            try managedContext.save()
        }catch let err{
            print(err)
        }
        
    }
    
    func retrieveCurrentNominal() -> Int{
        var currentBalance = 0
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CurrentNOminal")
        
        do{
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            result.forEach{ data in
                currentBalance = data.value(forKey: "currentNominal") as! Int
            }
        }catch let err{
            print(err)
        }
        
        return currentBalance
        
    }
    
    func retrieveCurrentBalanceId() -> String{
        var currentBalanceId = ""
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CurrentNomanl")
        
        do{
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            result.forEach{ data in
                currentBalanceId = data.value(forKey: "nominalId") as! String
            }
        }catch let err{
            print(err)
        }
        
        return currentBalanceId
        
    }
    func updateCurrentNominal(newValue: Int, nominalId: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Nominal> = Nominal.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nominalId = %@", nominalId)

        do {
            let results = try managedContext.fetch(fetchRequest)
            if let currentBalance = results.first {
                
            currentBalance.setValue(newValue, forKey: "currentNominal")

            try managedContext.save()
            }
        } catch let error as NSError {
            print("Error updating currentNominal: \(error), \(error.userInfo)")
        }
    }
    
}

